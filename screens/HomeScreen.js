import React, { Fragment, useState } from 'react';
import {SafeAreaView, ScrollView, View, Text, StatusBar, TextInput, Button, Picker} from 'react-native';
import * as firebase from 'firebase'

    /*signOutUser = () => {
        firebase.auth().signOut();
    }
    render(){
        return(
            <View style={styles.container}>
                <TouchableOpacity style={{marginTop: 32}} onPress={this.signOutUser}>
                    <Text>Logout</Text>
                </TouchableOpacity>
            </View>
        );
    }*/

    const HomeScreen = () => {

        const [inputText, setText] = useState('');
        const [responseText, setResponse] = useState('');

        function handleSignOut(){
            firebase.auth().signOut()
        };

         
      
        function postTranslateService(text) {
          fetch('https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20200421T123709Z.dbe7fba9c825cffb.000599e1737c34daa4fc02a27d1df6ba07ccbe5d&lang=tr-en&text=' + text)
            .then((res) => res.json())
            .then((res) => {
              console.log(res.text)
              setResponse(res.text)
            })
            .catch((error) => {
              console.log(error)
            });
        };
      
        return (
          <Fragment>
            <StatusBar barStyle="dark-content" />
            <SafeAreaView>
              <ScrollView
                contentInsetAdjustmentBehavior="automatic">
      
                <View>
                  <Text> TR - EN </Text>
                  <Picker>
                                              
                  </Picker>
                  <TextInput
                    numberOfLines={5}
                    multiline={true}
                    style={{ height: 120, borderColor: 'gray', borderWidth: 1 }}
                    onChangeText={text => setText(text)}
                  />
                  <Button
                    title="Translate"
                    color="#E9446A"
                    onPress={() => postTranslateService(inputText)}
                  />
                  <Button
                    title="Signout"
                    color="#414959"
                    onPress={() => handleSignOut()}
                  />
                  <Text>{'Response: ' +responseText}</Text>
                </View>
              </ScrollView>
            </SafeAreaView>            
          </Fragment>

        );
      };
      
      export default HomeScreen;