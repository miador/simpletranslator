import {createAppContainer, createSwitchNavigator} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import LoadingScreen from './screens/LoadingScreen'
import LoginScreen from './screens/LoginScreen'
import RegisterScreen from './screens/RegisterScreen'
import HomeScreen from './screens/HomeScreen'

import * as firebase from 'firebase'

var firebaseConfig = {
  apiKey: "AIzaSyCaN3jXONYPx4nm9sxdFIv2a06LOaO2kXU",
  authDomain: "socialapp-8ea3b.firebaseapp.com",
  databaseURL: "https://socialapp-8ea3b.firebaseio.com",
  projectId: "socialapp-8ea3b",
  storageBucket: "socialapp-8ea3b.appspot.com",
  messagingSenderId: "459878706572",
  appId: "1:459878706572:web:682e5f4edcffa878a67aa9"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const AppStack = createStackNavigator({
  Home: HomeScreen
})

const AuthStack = createStackNavigator({
  Login: LoginScreen,
  Register: RegisterScreen
})

export default createAppContainer(
  createSwitchNavigator(
    {
      Loading: LoadingScreen,
      App: AppStack,
      Auth: AuthStack
    },
    {
      initialRouteName: "Loading"
    }
  )
)